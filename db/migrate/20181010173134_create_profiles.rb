class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :bio
      t.string :ocupation
      t.integer :multiplier
      t.decimal :donation_amount
      t.string :currency
      t.references :users

      t.timestamps
    end
  end
end
